import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';

typedef CallBack = void Function();

enum HomePage{
  home,
  category,
  notification,
  account
}
class WidgetCustomTab extends StatefulWidget {
  @override
  _WidgetCustomTabState createState() => _WidgetCustomTabState();
  final String image;
  final String text;
  final HomePage page;
  final HomePage choose;
  final CallBack onTap;

  const WidgetCustomTab(
      {Key key, this.image, this.text, this.page, this.choose, this.onTap})
      : super(key: key);
}

class _WidgetCustomTabState extends State<WidgetCustomTab> {

  @override
  Widget build(BuildContext context) {
    Color color;
    bool checkIndex = widget.page == widget.choose;
    if (checkIndex) {
      color = AppColors.danger;
    } else {
      color = AppColors.grey;
    }

    var style = AppStyles.DEFAULT_SMALL.copyWith(color: color);
    return GestureDetector(
      onTap: () {
        checkIndex ? null : widget.onTap();
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: 25,
            height: 25,
            child: Image.asset(
              widget.image,
              fit: BoxFit.fill,
              color: color,
            ),
          ),
          Text(
            "${widget.text}",
            style: style,
          )
        ],
      ),
    );
  }
}
