import 'dart:async';

import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:rx_shared_preferences/rx_shared_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class AppShared {

  AppShared._();

  static final prefs = RxSharedPreferences(SharedPreferences.getInstance());

  static const String keyAccessToken = "keyViSafeAccessToken";
  static const String keyAccount = "keyAccount";


  static Future<bool> setAccessToken(String token) =>
      prefs.setString(keyAccessToken, token);

  static Future<String> getAccessToken() => prefs.getString(keyAccessToken);

  static Future<bool> setAccount(User data){
    String val = data != null ? json.encode(data.toJson()) : "" ;
    prefs.setString(keyAccount, val);
  }

  static Future<String> getAccount() => prefs.getString(keyAccount);
}
