import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

typedef Func = void Function();

class InstallmentScreen extends StatefulWidget {
  final Product params;

  const InstallmentScreen(this.params, {Key key}) : super(key: key);

  @override
  _InstallmentScreenState createState() => _InstallmentScreenState();
}

class _InstallmentScreenState extends State<InstallmentScreen> {
  InstallmentViewModel _viewModel;
  @override
  Widget build(BuildContext context) {
    return BaseWidget<InstallmentViewModel>(
      viewModel: InstallmentViewModel(
          repository: Provider.of(context), product: widget.params),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel;
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primary,
            title: Text(
              "Đặt mua trả góp",
              style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.light),
            ),
            leading: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Icon(
                Icons.arrow_back,
                color: AppColors.light,
              ),
            ),
          ),
          body: Container(
            child: FutureBuilder(
              future: _viewModel.init(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting)
                  return Center(
                    child: LoadingBouncingLine.circle(
                      size: 100,
                      backgroundColor: Colors.grey,
                    ),
                  );
                if (snapshot.data == null)
                  return _buildReloadPage();
                return _buildLayout(snapshot.data);
              },
            ),
          ),
        );
      },
    );
  }

  _buildLayout(dynamic data) {
    return Container(
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            _buildHeader(),
            Divider(
              color: AppColors.grey,
              height: 10,
              thickness: 10,
            ),
            _buildBody(data),
            _buildSubmitButton(),
            SizedBox(
              height: 48,
            )
          ],
        ),
      ),
    );
  }

  _buildHeader() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            flex: 4,
            child: Image.network(
                AppEndpoint.BASE_LOGIN + widget.params.imageSource),
          ),
          Expanded(
            child: SizedBox(),
            flex: 1,
          ),
          Expanded(
            flex: 6,
            child: Container(
              height: 120,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.params.name,
                    style: AppStyles.DEFAULT_LARGE_BOLD,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${NumberFormat(",###", "vi").format(widget.params.salePrice ?? widget.params.price)}",
                        style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                          color: AppColors.danger,
                        ),
                      ),
                      Text(
                        "đ",
                        style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                          color: AppColors.danger,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _buildBody(dynamic data) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
      child: Column(
        children: [
          _buildInstallmentPayment(),
          _buildListBank(data["banks"]),
          _buildListCreditCard(data["credit_cards"]),
          _buildListInstallmentMonths(_viewModel.product.installmentMonths),
          _buildListInstallmentPrepays(_viewModel.product.installmentPrepay),
          _buildInstallmentDetails(),
        ],
      ),
    );
  }

  _buildInstallmentPayment() {
    return Container(
      margin: const EdgeInsets.only(bottom: 14),
      height: 150,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildInstallmentStep("1", "Chọn loại hình trả góp"),
          Row(
            children: [
              Expanded(
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                  width: 150,
                  height: 100,
                  child: Center(
                    child: Text(
                      "Trả góp qua thẻ tín dụng",
                      style: AppStyles.DEFAULT_LARGE,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(width: 2, color: AppColors.primary)),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                  width: 150,
                  height: 100,
                  child: Center(
                    child: Text("Trả góp qua công ty tài chính",
                        style: AppStyles.DEFAULT_LARGE,
                        textAlign: TextAlign.center),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border: Border.all(width: 2, color: AppColors.grey)),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  _buildListBank(List<Bank> data) {
    List<Widget> list = [];

    for (var item in data) {
      list.add(StreamBuilder(
        stream: _viewModel.bankController.stream,
        builder: (context, snapshot) => GestureDetector(
          onTap: () => _viewModel.chooseBank(item.id),
          child: _buildCardItem(
              Image.network(AppEndpoint.BASE_LOGIN + item.imageSource),
              item.id,
              snapshot.data),
        ),
      ));
    }

    return Column(
      children: [
        _buildInstallmentStep("2", "Chọn ngân hàng phát hành thẻ"),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 14),
          child: Wrap(
            // crossAxisAlignment: WrapCrossAlignment.start,
            alignment: WrapAlignment.start,
            children: list,
            spacing: 10,
            runSpacing: 10,
          ),
        ),
      ],
    );
  }

  _buildListCreditCard(List<CreditCard> data) {
    List<Widget> list = [];

    for (var item in data) {
      list.add(StreamBuilder(
        stream: _viewModel.creditCardController.stream,
        builder: (context, snapshot) => GestureDetector(
          onTap: () => _viewModel.chooseCreditCard(item.id),
          child: _buildCardItem(
              Image.network(AppEndpoint.BASE_LOGIN + item.imageSource),
              item.id,
              snapshot.data),
        ),
      ));
    }

    return Column(
      children: [
        _buildInstallmentStep("3", "Chọn thẻ"),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 14),
          child: Wrap(
            // crossAxisAlignment: WrapCrossAlignment.start,
            alignment: WrapAlignment.start,
            children: list,
            spacing: 10,
            runSpacing: 10,
          ),
        ),
      ],
    );
  }

  _buildListInstallmentMonths(List<String> data) {
    List<Widget> list = [];

    for (var item in data) {
      list.add(StreamBuilder(
        stream: _viewModel.installmentMonthController.stream,
        builder: (context, snapshot) => GestureDetector(
          onTap: () => _viewModel.chooseInstallmentMonth(int.parse(item)),
          child: _buildCardItem(
              Text(
                "$item tháng",
                style: AppStyles.DEFAULT_MEDIUM_BOLD,
              ),
              int.parse(item),
              snapshot.data),
        ),
      ));
    }

    return Column(
      children: [
        _buildInstallmentStep("4", "Chọn tháng trả trước"),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 14),
          child: Wrap(
            // crossAxisAlignment: WrapCrossAlignment.start,
            alignment: WrapAlignment.start,
            children: list,
            spacing: 10,
            runSpacing: 10,
          ),
        ),
      ],
    );
  }

  _buildListInstallmentPrepays(List<String> data) {
    List<Widget> list = [];

    for (var item in data) {
      list.add(StreamBuilder(
        stream: _viewModel.installmentPrepaysController.stream,
        builder: (context, snapshot) => GestureDetector(
          onTap: () => _viewModel.chooseInstallmentPrepay(int.parse(item)),
          child: _buildCardItem(
              Text(
                "$item %",
                style: AppStyles.DEFAULT_MEDIUM_BOLD,
              ),
              int.parse(item),
              snapshot.data),
        ),
      ));
    }

    return Column(
      children: [
        _buildInstallmentStep("5", "Trả trước"),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 14),
          child: Wrap(
            // crossAxisAlignment: WrapCrossAlignment.start,
            alignment: WrapAlignment.start,
            children: list,
            spacing: 10,
            runSpacing: 10,
          ),
        ),
      ],
    );
  }

  _buildInstallmentDetails() {
    return Container(
      height: 120,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: 75,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _buildInstallDetailItem(
                    label: "Giá sản phẩm",
                    value: double.parse(
                        (widget.params.salePrice ?? widget.params.price)
                            .toString()),
                    labelStyle: AppStyles.DEFAULT_MEDIUM,
                    valueStyle: AppStyles.DEFAULT_MEDIUM,
                    unitStyle: AppStyles.DEFAULT_SMALL),
                _buildInstallDetailItem(
                    label: "Trả góp mỗi tháng",
                    value: 1,
                    labelStyle: AppStyles.DEFAULT_MEDIUM,
                    valueStyle: AppStyles.DEFAULT_MEDIUM,
                    unitStyle: AppStyles.DEFAULT_SMALL),
                StreamBuilder(
                  stream: _viewModel.prepayPriceController.stream,
                  builder: (context, snapshot) {
                    if (snapshot.data == null ||
                        snapshot.connectionState == ConnectionState.waiting)
                      return Container();
                    return _buildInstallDetailItem(
                        label: "Trả trước",
                        value: snapshot.data,
                        labelStyle: AppStyles.DEFAULT_MEDIUM,
                        valueStyle: AppStyles.DEFAULT_MEDIUM,
                        unitStyle: AppStyles.DEFAULT_SMALL);
                  },
                ),
              ],
            ),
          ),
          Divider(
            color: AppColors.grey,
            height: 2,
            thickness: 2,
          ),
          _buildInstallDetailItem(
              label: "TỔNG",
              value: double.parse(
                  (widget.params.salePrice ?? widget.params.price).toString()),
              labelStyle: AppStyles.DEFAULT_LARGE_BOLD,
              valueStyle: AppStyles.DEFAULT_LARGE_BOLD
                  .copyWith(color: AppColors.danger),
              unitStyle: AppStyles.DEFAULT_MEDIUM_BOLD
                  .copyWith(color: AppColors.danger)),
        ],
      ),
    );
  }

  _buildSubmitButton() {
    return GestureDetector(
      onTap: () async => await _viewModel.submitInstallment(),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 12),
        padding: const EdgeInsets.symmetric(vertical: 16),
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: Colors.red),
        child: Center(
          child: Text(
            "Chọn trả góp",
            style:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColors.light),
          ),
        ),
      ),
    );
  }

  _buildInstallDetailItem(
      {String label,
      double value,
      TextStyle labelStyle,
      TextStyle valueStyle,
      TextStyle unitStyle}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: labelStyle,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${NumberFormat(",###", "vi").format(value)}",
              style: valueStyle,
            ),
            Text(
              "đ",
              style: unitStyle,
            ),
          ],
        )
      ],
    );
  }

  _buildInstallmentStep(String number, String action) {
    return Row(
      children: [
        Container(
          width: 32,
          height: 32,
          child: Center(
            child: Text(
              number,
              style: AppStyles.DEFAULT_LARGE.copyWith(color: Colors.white),
            ),
          ),
          decoration: BoxDecoration(
              color: AppColors.primary,
              borderRadius: BorderRadius.all(Radius.circular(10))),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          action,
          style: AppStyles.DEFAULT_MEDIUM,
        )
      ],
    );
  }

  _buildCardItem(Widget widget, int index, int checkIndex) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
      width: 105,
      height: 75,
      child: Center(child: widget),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(
              width: 2,
              color: index == checkIndex ? AppColors.primary : AppColors.grey)),
    );
  }

  _buildReloadPage(){
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Có lỗi xảy ra vui lòng kiểm tra lại kết nối internet!",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
            GestureDetector(
              onTap: (){
                setState(() {

                });
              },
              child: Container(
                width: 100,
                height: 30,
                child: Center(
                  child: Text("Tải lại"),
                ),
                decoration: BoxDecoration(
                    color: Colors.grey
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
