import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/presentation/login/login.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';

import 'navigation/navigation_screen.dart';

class Routers {
  static const String navigation = "/";
  static const String login = "/login";
  static const String register = "/register";
  static const String otp = "/otp";
  static const String home = "/home";
  static const String product_details = "/product_details";
  static const String installment = "/installment";
  static const String installment_details = "/installment_details";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case navigation:
        return animRoute(NavigationScreen(), name: navigation);
        break;
      case login:
        return animRoute(LoginScreen(), name: login);
        break;
      case register:
        return animRoute(RegisterScreen(), name: register);
        break;
      case otp:
        return animRoute(OtpScreen(), name: otp);
        break;
      case home:
        return animRoute(PageScreen(arguments), name: home);
        break;
      case product_details:
        return animRoute(ProductDetailsScreen(arguments), name: product_details);
        break;
      case installment:
        return animRoute(InstallmentScreen(arguments), name: installment);
        break;
      case installment_details:
        return animRoute(InstallmentDetailsScreen(params: arguments,), name: installment_details);
        break;
      default:
        return animRoute(Container(
            child:
                Center(child: Text('No route defined for ${settings.name}'))));
    }
  }

  static Route animRoute(Widget page,
      {Offset beginOffset, String name, Object arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(0.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Offset center = Offset(0.0, 0.0);
  static Offset top = Offset(0.0, 1.0);
  static Offset bottom = Offset(0.0, -1.0);
  static Offset left = Offset(-1.0, 0.0);
  static Offset right = Offset(1.0, 0.0);
}
