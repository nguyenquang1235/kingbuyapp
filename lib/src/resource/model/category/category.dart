import 'category_model.dart';

class Category {
  Category({
    this.id,
    this.name,
    this.imageSource,
    this.iconSource,
    this.backgroundImage,
    this.parentId,
    this.parent,
    this.children,
    this.description,
    this.slug,
    this.videoLink,
  });

  int id;
  String name;
  String imageSource;
  String iconSource;
  String backgroundImage;
  int parentId;
  Parent parent;
  List<Category> children;
  String description;
  String slug;
  String videoLink;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        name: json["name"],
        imageSource: json["image_source"] ?? "",
        iconSource: json["icon_source"] == null ? null : json["icon_source"],
        backgroundImage:
            json["background_image"] == null ? null : json["background_image"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        parent: json["parent"] == null ? null : Parent.fromJson(json["parent"]),
        children: json["children"] != null ? List<Category>.from(
            json["children"].map((x) => Category.fromJson(x))) : null,
        description: json["description"] == null ? null : json["description"],
        slug: json["slug"],
        videoLink: json["video_link"] == null ? null : json["video_link"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image_source": imageSource,
        "icon_source": iconSource == null ? null : iconSource,
        "background_image": backgroundImage == null ? null : backgroundImage,
        "parent_id": parentId == null ? null : parentId,
        "parent": parent == null ? null : parent.toJson(),
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
        "description": description == null ? null : description,
        "slug": slug,
        "video_link": videoLink == null ? null : videoLink,
      };
}
