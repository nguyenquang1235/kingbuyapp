import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:kingbuy_app/src/presentation/page/page.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';

class PageScreen extends StatefulWidget {

  final dynamic params;
  const PageScreen(this.params);

  @override
  _PageScreenState createState() => _PageScreenState();
}

class _PageScreenState extends State<PageScreen> with TickerProviderStateMixin {
  PageViewModel _viewModel;

  @override
  void initState() {
    _viewModel = PageViewModel();
    _viewModel.init(widget.params);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<PageViewModel>(
      viewModel: PageViewModel(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Container(
            child: child,
          ),
        );
      },
      child: _buildLayoutHomePage(),
    );
  }

  _buildLayoutHomePage() {
    return Column(
      children: [
        Expanded(
          child: StreamBuilder(
            stream: _viewModel.widgetStream,
            builder: (context, snapshot) {
              return Container(
                child: snapshot.data,
              );
            }
          ),
          flex: 9,
        ),
        Expanded(
          child: _buildBottomMenu(),
          flex: 1,
        )
      ],
    );
  }

  _buildBottomMenu() {
    return Stack(
      alignment: Alignment(0, 0),
      children: [
        StreamBuilder(
            stream: _viewModel.pageStream,
            builder: (context, snapshot) {
              return Container(
                padding: const EdgeInsets.all(12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: WidgetCustomTab(
                        text: "Trang chủ",
                        page: HomePage.home,
                        choose: snapshot.data,
                        image: AppImages.home,
                        onTap: () => _viewModel.pushPage(HomePage.home),
                      ),
                    ),
                    Expanded(
                      child: WidgetCustomTab(
                        text: "Danh mục",
                        page: HomePage.category,
                        choose: snapshot.data,
                        image: AppImages.menu,
                        onTap: () => _viewModel.pushPage(HomePage.category),
                      ),
                    ),
                    Expanded(child: SizedBox()),
                    Expanded(
                      child: WidgetCustomTab(
                        text: "Thông báo",
                        page: HomePage.notification,
                        choose: snapshot.data,
                        image: AppImages.alarm,
                        onTap: () => _viewModel.pushPage(HomePage.notification),
                      ),
                    ),
                    Expanded(
                      child: WidgetCustomTab(
                        text: "Tài khoản",
                        page: HomePage.account,
                        choose: snapshot.data,
                        image: AppImages.user2,
                        onTap: () => _viewModel.pushPage(HomePage.account),
                      ),
                    )
                  ],
                ),
                decoration: BoxDecoration(
                    // border: Border(top: BorderSide(width: 0.5)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black,
                        blurRadius: 1.0,
                        spreadRadius: 0.0,
                        offset: Offset(0, 2), //
                      ),
                      BoxShadow(
                        color: Colors.white,
                        blurRadius: 1.0,
                        spreadRadius: 0.0,
                        offset: Offset(0, 2), //
                      )
                    ]),
              );
            }),
        Image.asset(AppImages.redPoint),
      ],
    );
  }
}
