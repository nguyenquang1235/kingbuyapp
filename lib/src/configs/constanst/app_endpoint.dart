class AppEndpoint {
  AppEndpoint._();

  static const String BASE_URL = "http://kingbuy.sapp.asia";
  static const String BASE_LOGIN = "https://kingbuy.sapp.asia";

  static const int connectionTimeout = 1500;
  static const int receiveTimeout = 1500;
  static const String keyAuthorization = "Authorization";

  static const int SUCCESS = 200;
  static const int ERROR_TOKEN = 401;
  static const int ERROR_VALIDATE = 422;
  static const int ERROR_SERVER = 500;
  static const int ERROR_DISCONNECT = -1;

  //Login
  static const String LOGIN = '/api/loginApp';
  static const String LOGIN_FACEBOOK = '/api/loginAppSocial';
  static const String LOGIN_GOOGLE = '/api/loginAppGoogle';
  static const String LOGIN_APPLE = '/api/loginWithApple';

  //KhuyenMai
  static const String GET_COUPON = '/api/getMyCoupons';
  static const String GET_DETAIL_COUPON = '/api/getDetailCoupon';

  //TrangChu
  static const String GET_ALL_PROMOTION = '/api/getAllPromotion';
  static const String GET_ALL_CATEGORY = '/api/getAllCategories';
  static const String GET_ALL_MY_PROMOTION = '/api/getAllMyPromotion';
  static const String GET_PRODUCT_BY_CATEGORY = '/api/getProductsByCategory';
  static const String GET_NEW_PRODUCTS = '/api/getAllProductNew';
  static const String GET_SELLING_PRODUCTS = '/api/getAllProductSelling';
  static const String POPUP = '/api/getPopup';

  //Tragop
  static const String GET_ALL_CREDIT_CARD = '/api/creditList';
  static const String GET_ALL_BANK = '/api/banks';
  static const String CREATE_INSTALLMENT = '/api/createInstallment';
  static const String DETAIL_INSTALLMENT = '/api/detailInstallment';
}
