class Parent {
  Parent({
    this.id,
    this.name,
    this.slug,
    this.videoLink,
    this.parent,
  });

  int id;
  String name;
  String slug;
  String videoLink;
  dynamic parent;

  factory Parent.fromJson(Map<String, dynamic> json) => Parent(
    id: json["id"],
    name: json["name"],
    slug: json["slug"],
    videoLink: json["video_link"],
    parent: json["parent"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "slug": slug,
    "video_link": videoLink,
    "parent": parent,
  };
}
