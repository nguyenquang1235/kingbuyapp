import 'package:intl/intl.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/resource/model/category/category.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:rxdart/rxdart.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import '../presentation.dart';

class WidgetProductCategory extends StatefulWidget {
  @override
  _WidgetProductCategoryState createState() => _WidgetProductCategoryState();

  final Category category;
  final List<Product> products;
  const WidgetProductCategory({this.category, this.products});
}

class _WidgetProductCategoryState extends State<WidgetProductCategory> {
  final _tabController = BehaviorSubject<String>();
  final _productsController = BehaviorSubject<List<Product>>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onChangeCategory(
        widget.category.children.first.name, widget.category.children.first.id);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.category.name,
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          _buildChildCategory(),
          _buildProducts()
        ],
      ),
    );
  }

  _buildChildCategory() {
    List<Widget> list = [];

    for (var item in widget.category.children) {
      list.add(
        StreamBuilder<Object>(
            stream: _tabController.stream,
            builder: (context, snapshot) {
              return GestureDetector(
                onTap: () => _onChangeCategory(item.name, item.id),
                child: Container(
                  margin: EdgeInsets.all(8),
                  padding: const EdgeInsets.all(4),
                  width: 150,
                  height: 50,
                  child: Text(
                    item.name,
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: snapshot.data == item.name
                            ? AppColors.light
                            : AppColors.dark),
                    textAlign: TextAlign.center,
                  ),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          spreadRadius: 0.0,
                          blurRadius: 6.0,
                          offset: Offset(0, 0),
                        )
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      // border: Border.all(width: 0.05),
                      color: snapshot.data == item.name
                          ? AppColors.primary
                          : AppColors.light),
                ),
              );
            }),
      );
    }

    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: list,
      ),
    );
  }

  _buildProducts() {
    return StreamBuilder(
      stream: _productsController.stream,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting)
          return LoadingBouncingLine.circle(
            size: 50,
            backgroundColor: Colors.grey,
          );
        if (snapshot.data.length == 0)
          return Center(
            child: Text("Không có sản phẩm"),
          );
        return _buildProductItem(snapshot.data);
      },
    );
  }

  _buildProductItem(List<Product> product) {
    List<Widget> list = [];

    for (var item in product) {
      list.add(
        GestureDetector(
          onTap: () => _getProductDetails(item),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.6,
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 4),
            margin: const EdgeInsets.all(2),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(0, 1))
                ],
                border:
                    Border.all(width: 0.5, color: Colors.grey.withOpacity(0.5))),
            child: Stack(
              children: [
                Column(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.35,
                      height: MediaQuery.of(context).size.width * 0.35,
                      child: Image.network(
                        AppEndpoint.BASE_LOGIN + item.imageSource,
                        fit: BoxFit.fill,
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return LoadingBouncingLine.circle(
                            size: 50,
                            backgroundColor: Colors.grey,
                          );
                        },
                      ),
                    ),
                    ReadMoreText(
                      item.name.toUpperCase(),
                      trimLines: 2,
                      trimMode: TrimMode.Line,
                      trimCollapsedText: '...',
                      colorClickableText: Colors.black,
                      style: AppStyles.DEFAULT_MEDIUM_BOLD,
                      textAlign: TextAlign.start,
                    ),
                    Align(
                      alignment: Alignment(-1, 0),
                      child: Text(
                        "${item.brandName}",
                        style: AppStyles.DEFAULT_SMALL_BOLD
                            .copyWith(color: AppColors.primary),
                      ),
                    ),
                    Align(
                      alignment: Alignment(-1, 0),
                      child: SmoothStarRating(
                          allowHalfRating: false,
                          starCount: 5,
                          rating: double.parse(item.star.toString()),
                          size: 15,
                          isReadOnly: true,
                          filledIconData: Icons.star,
                          halfFilledIconData: Icons.star_half,
                          color: Colors.orange,
                          borderColor: Colors.orange,
                          spacing: 0.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${NumberFormat(",###", "vi").format(item.price)}",
                              style: AppStyles.DEFAULT_MEDIUM_BOLD
                                  .copyWith(color: AppColors.danger),
                            ),
                            Text(
                              "đ",
                              style: AppStyles.DEFAULT_SMALL_BOLD
                                  .copyWith(color: AppColors.danger),
                            ),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${NumberFormat(",###", "vi").format(item.salePrice)}",
                              style: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
                                  color: AppColors.dark,
                                  decoration: TextDecoration.lineThrough),
                            ),
                            Text(
                              "đ",
                              style: AppStyles.DEFAULT_VERY_SMALL_BOLD.copyWith(
                                  color: AppColors.dark,
                                  decoration: TextDecoration.lineThrough),
                            )
                          ],
                        )
                      ],
                    ),
                    item.gifts.isNotEmpty
                        ? Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Image.asset(AppImages.gift1),
                                flex: 2,
                              ),
                              Expanded(
                                child: SizedBox(),
                                flex: 1,
                              ),
                              Expanded(
                                flex: 9,
                                child: ReadMoreText(
                                  "Tặng ${item.gifts.first.name}",
                                  trimLines: 1,
                                  trimMode: TrimMode.Line,
                                  trimCollapsedText: '...',
                                  colorClickableText: Colors.black,
                                  style: AppStyles.DEFAULT_SMALL_BOLD,
                                  textAlign: TextAlign.start,
                                ),
                              ),
                            ],
                          )
                        : Container(),
                  ],
                ),
                Align(
                  alignment: Alignment(1, -1),
                  child: item.saleOff != 0
                      ? Container(
                          width: 40,
                          height: 20,
                          alignment: Alignment(0, 0),
                          decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(30)),
                              color: Colors.red),
                          child: Text(
                            "-${item.saleOff}%",
                            style: AppStyles.DEFAULT_MEDIUM
                                .copyWith(color: AppColors.light),
                          ),
                        )
                      : null,
                )
              ],
            ),
          ),
        ),
      );
    }

    return Container(
      height: 270,
      child: ListView(
        children: list,
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  _onChangeCategory(String name, int id) {
    // print(id);
    _tabController.sink.add(name);
    _productsController.sink.add(widget.products
        .where((element) => element.productCategoryId == id)
        .toList());
  }

  _getProductDetails(Product product){
    Navigator.pushNamed(context, Routers.product_details, arguments: product);
  }
}
