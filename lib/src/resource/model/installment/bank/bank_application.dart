import 'bank_model.dart';

class BankApplication {
  BankApplication({
    this.data,
  });

  List<Bank> data;

  BankApplication.fromJson(List<dynamic> json) {
    data = [];
    for(var item in json){
      data.add(Bank.fromJson(item));
    }
  }
}