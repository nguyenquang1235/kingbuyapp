class ProductColor {
  ProductColor({
    this.id,
    this.name,
    this.imageSource,
  });

  int id;
  String name;
  List<String> imageSource;

  factory ProductColor.fromJson(Map<String, dynamic> json) => ProductColor(
    id: json["id"],
    name: json["name"],
    imageSource: List<String>.from(json["image_source"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image_source": List<dynamic>.from(imageSource.map((x) => x)),
  };
}
