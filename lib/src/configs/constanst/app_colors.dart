import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static final Color primary = Colors.blue;
  static final Color grey = Colors.grey[300];
  static final Color light = Colors.white;
  static const Color dark = Colors.black;
  static final Color danger = Colors.red;
  static final Color enableButton = Color.fromRGBO(246, 133, 14, 1);
  static final Color disableButton = Color.fromRGBO(246, 133, 14, 0.65);
}
