import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/utils/app_shared.dart';
import '../base/base.dart';
import '../routers.dart';

class SplashViewModel extends BaseViewModel{

  init()async{
    // if(await checkAccessToken()){
    //   await Future.delayed(const Duration(seconds: 2), ()=>Navigator.pushReplacementNamed(context, Routers.login));
    // }else{
    //   await Future.delayed(const Duration(seconds: 2), ()=>Navigator.pushReplacementNamed(context, Routers.home));
    // }
    await Future.delayed(const Duration(seconds: 2), ()=>Navigator.pushReplacementNamed(context, Routers.login));
  }

  checkAccessToken()async{
    return(await AppShared.getAccessToken() == null);
  }
}