import 'package:kingbuy_app/src/resource/model/model.dart';

class MyPromotionApplication {
  MyPromotionApplication({
    this.promotions,
    this.recordsTotal,
  });

  List<MyPromotion> promotions;
  int recordsTotal;

  factory MyPromotionApplication.fromJson(Map<String, dynamic> json) => MyPromotionApplication(
    promotions: List<MyPromotion>.from(json["promotions"].map((x) => MyPromotion.fromJson(x))),
    recordsTotal: json["recordsTotal"],
  );

  Map<String, dynamic> toJson() => {
    "promotions": List<dynamic>.from(promotions.map((x) => x.toJson())),
    "recordsTotal": recordsTotal,
  };
}