import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';
import 'package:kingbuy_app/src/resource/repo/home_repository.dart';

class HomeViewModel extends BaseViewModel {
  final HomeRepository repository;
  HomeViewModel({this.repository});
}
