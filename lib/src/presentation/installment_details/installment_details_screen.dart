import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/resource/resource.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import '../presentation.dart';

class InstallmentDetailsScreen extends StatefulWidget {
  final dynamic params;

  const InstallmentDetailsScreen({Key key, this.params}) : super(key: key);
  @override
  _InstallmentDetailsScreenState createState() =>
      _InstallmentDetailsScreenState();
}

class _InstallmentDetailsScreenState extends State<InstallmentDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<InstallmentDetailsViewModel>(
      viewModel: InstallmentDetailsViewModel(
        repository: Provider.of(context),
        invoiceId: widget.params,
      ),
      builder: (context, viewModel, child) {
        return Scaffold(
            appBar: AppBar(
              backgroundColor: AppColors.primary,
              title: Text(
                "Thông tin trả góp",
                style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.light),
              ),
              leading: GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Icon(
                  Icons.arrow_back,
                  color: AppColors.light,
                ),
              ),
            ),
            body: FutureBuilder(
              future: viewModel.init(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: LoadingBouncingLine.circle(
                      size: 100,
                      backgroundColor: Colors.grey,
                    ),
                  );
                }
                if(snapshot.data == null)
                  return _buildReloadPage();
                return Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.grey.withOpacity(0.2),
                  child: Column(
                    children: [
                      _buildHeader(snapshot.data),
                      _buildBody(snapshot.data)
                    ],
                  ),
                );
              },
            ));
      },
    );
  }

  _buildHeader(InstallmentDetails details) {
    return Container(
      margin: const EdgeInsets.all(16),
      padding: const EdgeInsets.all(16),
      height: 120,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _buildInstallDetailMoney(
            label: "Giá sản phẩm",
            value: details.total ?? 0,
            labelStyle:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
            valueStyle:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
            unitStyle:
                AppStyles.DEFAULT_SMALL_BOLD.copyWith(color: Colors.white),
          ),
          _buildInstallDetailMoney(
            label: "Trả góp mỗi tháng",
            value: details.payMonthlyInstallment ?? 0,
            labelStyle:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
            valueStyle:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
            unitStyle:
                AppStyles.DEFAULT_SMALL_BOLD.copyWith(color: Colors.white),
          ),
          _buildInstallDetailMoney(
            label: "Trả trước",
            value: details.prepayInstallmentAmount ?? 0,
            labelStyle:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
            valueStyle:
                AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
            unitStyle:
                AppStyles.DEFAULT_SMALL_BOLD.copyWith(color: Colors.white),
          )
        ],
      ),
      decoration: BoxDecoration(
          color: AppColors.primary,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.5),
                spreadRadius: 0.1,
                blurRadius: 1,
                offset: Offset(1, 3))
          ]),
    );
  }

  _buildBody(InstallmentDetails details) {
    return Container(
      width: MediaQuery.of(context).size.width,
      color: Colors.white,
      height: MediaQuery.of(context).size.height / 2.4,
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(top: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Thông tin trả góp".toUpperCase(),
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          _buildInstallDetailsItem(
            label: "Loại hình trả góp",
            childValue: Text(
              details.installmentType == 1
                  ? "Trả góp qua thẻ tín dụng"
                  : "Trả góp qua công ty tài chính",
              style: AppStyles.DEFAULT_SMALL,
            ),
          ),
          _buildInstallDetailsItem(
            label: "Ngân hàng phát hành thẻ",
            childValue: _buildImage(details.bankImage),
          ),
          _buildInstallDetailsItem(
            label: "Loại thẻ",
            childValue: _buildImage(details.installmentCardImage),
          ),
          _buildInstallDetailsItem(
              label: "Gói trả góp",
              childValue: Text(
                "${details.monthsInstallment} tháng",
                style: AppStyles.DEFAULT_SMALL,
              )),
          _buildInstallDetailsItem(
              label: "Trả trước",
              childValue: Text(
                "${details.prepayInstallment} %",
                style: AppStyles.DEFAULT_SMALL,
              )),
        ],
      ),
    );
  }

  _buildInstallDetailMoney(
      {String label,
      int value,
      TextStyle labelStyle,
      TextStyle valueStyle,
      TextStyle unitStyle}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: labelStyle,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${NumberFormat(",###", "vi").format(value)}",
              style: valueStyle,
            ),
            Text(
              "đ",
              style: unitStyle,
            ),
          ],
        )
      ],
    );
  }

  _buildInstallDetailsItem({String label, Widget childValue}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          label,
          style: AppStyles.DEFAULT_MEDIUM,
        ),
        childValue ?? Container()
      ],
    );
  }

  _buildImage(String ulr) {
    return Image.network(
      AppEndpoint.BASE_LOGIN + ulr,
      width: 60,
      height: 40,
      fit: BoxFit.fill,
    );
  }

  _buildReloadPage(){
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Có lỗi xảy ra vui lòng kiểm tra lại kết nối internet!",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
            GestureDetector(
              onTap: (){
                setState(() {

                });
              },
              child: Container(
                width: 100,
                height: 30,
                child: Center(
                  child: Text("Tải lại"),
                ),
                decoration: BoxDecoration(
                    color: Colors.grey
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
