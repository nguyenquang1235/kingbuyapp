import 'package:flutter/material.dart';

class Shop {
  int _id;
  String _address;
  String _hotLine;
  String _openingHours;
  String _imageSource;
  String _latitude;
  String _longitude;

  int get id => _id;
  String get address => _address;
  String get hotLine => _hotLine;
  String get openingHours => _openingHours;
  String get imageSource => _imageSource;
  String get latitude => _latitude;
  String get longitude => _longitude;

  Shop({
    int id,
    String address,
    String hotLine,
    String openingHours,
    String imageSource,
    String latitude,
    String longitude,
  }) {
    _id = id;
    _address = address;
    _hotLine = hotLine;
    _openingHours = openingHours;
    _imageSource = imageSource;
    _latitude = latitude;
    _longitude = longitude;
  }

  factory Shop.fromJson(Map<String, dynamic> data){
    return Shop(
      id: data["shop"]["id"],
      address: data["shop"]["address"],
      hotLine: data["shop"]["hot_line"],
      imageSource: data["shop"]["image_source"],
      latitude: data["shop"]["latitude"],
      longitude: data["shop"]["longitude"],
      openingHours: data["shop"]["opening_hours"]
    );
  }
}
