import 'package:kingbuy_app/src/resource/model/installment/credit_card/credit_card_model.dart';

class CreditApplication {

  CreditApplication({
    this.data,
  });

  List<CreditCard> data;

  CreditApplication.fromJson(List<dynamic> json) {
    data = [];
    for(var item in json){
      data.add(CreditCard.fromJson(item));
    }
  }
}