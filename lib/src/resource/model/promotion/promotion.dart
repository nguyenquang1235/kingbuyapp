class Promotion {
  int _id;
  String _title;
  String _imageSource;
  String _description;
  String _content;
  String _slug;
  int _isAutoSlug;
  int _status;
  String _seoTitle;
  String _seoDescription;
  int _checkPromotion;
  int _isFeatured;
  String _createdAt;
  String _updatedAt;
  int _order;

  Promotion({
    int id,
    String title,
    String imageSource,
    String description,
    String content,
    String slug,
    int isAutoSlug,
    int status,
    String seoTitle,
    String seoDescription,
    int checkPromotion,
    int isFeatured,
    String createdAt,
    String updatedAt,
    int order,
  }) {
    this._id = id;
    this._title = title;
    this._imageSource = imageSource;
    this._description = description;
    this._content = content;
    this._slug = slug;
    this._isAutoSlug = isAutoSlug;
    this._status = status;
    this._seoTitle = seoTitle;
    this._seoDescription = seoDescription;
    this._checkPromotion = checkPromotion;
    this._isFeatured = isFeatured;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
    this._order = _order;
  }

  int get id => _id;
  String get title => _title;
  String get imageSource => _imageSource;
  String get description => _description;
  String get content => _content;
  String get slug => _slug;
  int get isAutoSlug => _isAutoSlug;
  int get status => _status;
  String get seoTitle => _seoTitle;
  String get seoDescription => _seoDescription;
  int get checkPromotion => _checkPromotion;
  int get isFeatured => _isFeatured;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  int get order => _order;

  factory Promotion.fromJson(Map<String, dynamic> json) => Promotion(
    id: json["id"],
    title: json["title"],
    imageSource: json["image_source"],
    description: json["description"],
    content: json["content"],
    slug: json["slug"],
    isAutoSlug: json["is_auto_slug"],
    seoTitle: json["seo_title"],
    seoDescription: json["seo_description"],
    checkPromotion: json["check_promotion"],
    status: json["status"],
    isFeatured: json["is_featured"],
    order: json["order"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": _id,
    "title": _title,
    "image_source": _imageSource,
    "description": _description,
    "content": _content,
    "slug": _slug,
    "is_auto_slug": _isAutoSlug,
    "seo_title": _seoTitle,
    "seo_description": _seoDescription,
    "check_promotion": _checkPromotion,
    "status": _status,
    "is_featured": _isFeatured,
    "order": _order,
    "created_at": _createdAt,
    "updated_at": _updatedAt,
  };
}
