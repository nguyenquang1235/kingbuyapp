import 'package:dio/dio.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/presentation/login/login_viewmodel.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:kingbuy_app/src/resource/services/services.dart';
import 'package:kingbuy_app/src/utils/app_clients.dart';

class LoginRepository {
  Future<NetworkState<dynamic>> getUser(dynamic token, LoginType loginType) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      switch (loginType) {
        case LoginType.Facebook:
          Map<String, String> param = {"access_token": token};
          response = await AppClients(baseUrl: AppEndpoint.BASE_LOGIN)
              .post(AppEndpoint.LOGIN_FACEBOOK, data: param);
          break;
        case LoginType.Google:
          Map<String, String> param = {"access_token": token};
          response = await AppClients(baseUrl: AppEndpoint.BASE_LOGIN)
              .post(AppEndpoint.LOGIN_GOOGLE, data: param);
          break;
        case LoginType.Account:
          response = await AppClients(baseUrl: AppEndpoint.BASE_LOGIN)
              .post(AppEndpoint.LOGIN, data: token);
          break;
      }
      // print(response.data["data"]["token"]);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }
}
