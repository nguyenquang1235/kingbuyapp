import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:kingbuy_app/src/resource/model/network_state.dart';
import 'package:kingbuy_app/src/resource/repo/login_repository.dart';
import 'package:kingbuy_app/src/utils/utils.dart';
import 'package:toast/toast.dart';

import '../routers.dart';

enum LoginType { Facebook, Google, Account }

class LoginViewModel extends BaseViewModel {

  final LoginRepository repository;
  final TextEditingController account;
  final TextEditingController password;
  final formKey = GlobalKey<FormState>();
  final BuildContext context;

  LoginViewModel({@required this.repository, this.account, this.password, this.context});

  login(LoginType loginType) async {
    dynamic token;
    switch (loginType) {
      case LoginType.Facebook:
        final facebookLogin = await FacebookLogin().logIn(['email']);
        token = facebookLogin.accessToken.token;
        break;
      case LoginType.Google:
        final googleLogin = await GoogleSignIn(scopes: ['email']).signIn();
        token = (await googleLogin.authentication).accessToken;
        break;
      case LoginType.Account:
        // token = {
        //   "identity": account.value.text,
        //   "password": password.value.text
        // };
        token = {
          "identity": "0979629204",
          "password": "12345678"
        };
        break;
    }
    var data = await repository.getUser(token, loginType);
    await _checkAccount(data);
  }

  forgotPass() {
    print("forgot pass");
  }

  registerAcc() {
    Navigator.pushReplacementNamed(context, Routers.register);
  }

  _checkAccount(NetworkState<dynamic> result) async {
    if (result.status == 0) {
      Toast.show(result.message, context, duration: 3);
    } else {
      print(result.data["token"]);
      _saveAccessToken(result.data["token"]);
      Toast.show("Chào mừng ${result.data["profile"]["name"]}", context, duration: 3);
      var user = User.formJson(result.data);
      await AppShared.setAccount(user);
      Navigator.pushNamed(context, Routers.home, arguments: user);
    }
  }

  // String validateAccount(String value) {
  //   if (value.isEmpty) return "Email hoặc số điện thoại bắt buộc phải nhập";
  //   RegExp regPhone = new RegExp(r'^(?:[+0][1-9])?[0-9]{10,12}$');
  //   RegExp regEmail = new RegExp(
  //       r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  //   if (!regPhone.hasMatch(value) || !regEmail.hasMatch(value))
  //     return "Email hoặc số điện thoại không hợp lệ";
  //   return null;
  // }

  String validatePass(String value){
    if(value.isEmpty) return "Mật khẩu bắt buộc phải nhập";
    if(value.length < 8) return "Mật khẩu bắt buộc lớn hơn 8 kí tự";
    return null;
  }

  _saveAccessToken(String token){
    // _decodeToken(token);
    return AppShared.setAccessToken(token);
  }

  // _decodeToken(String token){
  //   var decodedToken = FirebaseAuth.instance.;
  //   print(decodedToken);
  // }

}
