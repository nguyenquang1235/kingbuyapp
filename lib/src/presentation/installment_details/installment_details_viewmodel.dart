import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:kingbuy_app/src/resource/repo/installment_repository.dart';
import 'package:kingbuy_app/src/resource/resource.dart';

class InstallmentDetailsViewModel extends BaseViewModel {
  final int invoiceId;
  final InstallmentRepository repository;

  InstallmentDetailsViewModel({
    this.invoiceId,
    this.repository,
  });

  init() async {
    var result = await repository.getInstallmentDetails(invoiceId);
    return InstallmentDetails.fromJson(result.data["detail"]);
  }
}
