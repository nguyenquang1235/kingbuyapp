import 'package:kingbuy_app/src/resource/model/product/gift.dart';
import 'package:kingbuy_app/src/resource/model/product/product_color.dart';

import '../../resource.dart';


class Product {
  Product({
    this.id,
    this.name,
    this.price,
    this.salePrice,
    this.productCategoryId,
    this.brandId,
    this.giftIds,
    this.imageSource,
    this.imageSourceList,
    this.description,
    this.content,
    this.specifications,
    this.videoLink,
    this.slug,
    this.status,
    this.goodsStatus,
    this.isBulky,
    this.isInstallment,
    this.installmentMonths,
    this.installmentPrepay,
    this.barcode,
    this.createdAt,
    this.updatedAt,
    this.productCategoryName,
    this.brandName,
    this.brandInfo,
    this.saleOff,
    this.gifts,
    this.star,
    this.colors,
    this.category,
  });

  Category category;
  int id;
  String name;
  int price;
  int salePrice;
  int productCategoryId;
  int brandId;
  List<int> giftIds;
  String imageSource;
  List<String> imageSourceList;
  String description;
  String content;
  String specifications;
  String videoLink;
  String slug;
  int status;
  int goodsStatus;
  int isBulky;
  int isInstallment;
  List<String> installmentMonths;
  List<String> installmentPrepay;
  String barcode;
  String createdAt;
  String updatedAt;
  String productCategoryName;
  String brandName;
  String brandInfo;
  int saleOff;
  List<Gift> gifts;
  int star;
  List<ProductColor> colors;

  factory Product.fromJson(Map<String, dynamic> json){
    return Product(
      category: Category.fromJson(json["category"]),
      id: json["id"],
      name: json["name"],
      price: json["price"],
      salePrice: json["sale_price"],
      productCategoryId: json["product_category_id"],
      brandId: json["brand_id"],
      giftIds: List<int>.from(json["gift_ids"].map((x) => x)),
      imageSource: json["image_source"],
      imageSourceList: List<String>.from(json["image_source_list"].map((x) => x)),
      description: json["description"],
      content: json["content"],
      specifications: json["specifications"],
      videoLink: json["video_link"] == null ? null : json["video_link"],
      slug: json["slug"],
      status: json["status"],
      goodsStatus: json["goods_status"],
      isBulky: json["is_bulky"],
      isInstallment: json["is_installment"],
      installmentMonths: List<String>.from(json["installment_months"].map((x) => x)),
      installmentPrepay: List<String>.from(json["installment_prepay"].map((x) => x)),
      barcode: json["barcode"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      productCategoryName: json["product_category_name"],
      brandName: json["brand_name"],
      brandInfo: json["brand_info"] == null ? null : json["brand_info"],
      saleOff: json["sale_off"],
      gifts: List<Gift>.from(json["gifts"].map((x) => Gift.fromJson(x))),
      star: json["star"],
      colors: List<ProductColor>.from(json["colors"].map((x) => ProductColor.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "price": price,
    "sale_price": salePrice,
    "product_category_id": productCategoryId,
    "brand_id": brandId,
    "gift_ids": List<dynamic>.from(giftIds.map((x) => x)),
    "image_source": imageSource,
    "image_source_list": List<dynamic>.from(imageSourceList.map((x) => x)),
    "description": description,
    "content": content,
    "specifications": specifications,
    "video_link": videoLink == null ? null : videoLink,
    "slug": slug,
    "status": status,
    "goods_status": goodsStatus,
    "is_bulky": isBulky,
    "is_installment": isInstallment,
    "installment_months": List<dynamic>.from(installmentMonths.map((x) => x)),
    "installment_prepay": List<dynamic>.from(installmentPrepay.map((x) => x)),
    "barcode": barcode,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "product_category_name": productCategoryName,
    "brand_name": brandName,
    "brand_info": brandInfo == null ? null : brandInfo,
    "sale_off": saleOff,
    "gifts": List<dynamic>.from(gifts.map((x) => x.toJson())),
    "star": star,
    "colors": List<dynamic>.from(colors.map((x) => x.toJson())),
  };
}
