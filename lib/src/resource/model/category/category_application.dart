import '../model.dart';

class CategoryApplication {
  CategoryApplication({
    this.categories,
  });

  List<Category> categories;
  int recordsTotal;

  factory CategoryApplication.fromJson(Map<String, dynamic> json) => CategoryApplication(
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
      };
}
