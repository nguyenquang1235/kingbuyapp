import 'package:dio/dio.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:kingbuy_app/src/resource/services/services.dart';
import 'package:kingbuy_app/src/utils/app_clients.dart';

class HomeRepository{
  
  Map<String, dynamic> data;
  
  init() async {
    data = {
      "promotions": PromotionApplication.fromJson((await getAllPromotion()).data).promotions,
      "my_promotions": MyPromotionApplication.fromJson((await getAllMyPromotion()).data).promotions,
      "categories": CategoryApplication.fromJson((await getAllCategory()).data).categories,
      "new_products": NewProductApplication.fromJson((await getNewProducts()).data).data,
    };
    await getAllProduct();
  }
  
  Future<NetworkState<dynamic>> getAllPromotion() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_ALL_PROMOTION);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> getAllCategory() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_ALL_CATEGORY);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> getAllMyPromotion() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_ALL_MY_PROMOTION);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> getNewProducts() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      Map<String, String> param = {"product_category_id": "", "limit": "20", "offset": "1"};
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_NEW_PRODUCTS, queryParameters: param);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> getProductByCategory(int id) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      Map<String, String> param = {"product_category_id": "$id", "limit": "100", "offset": "0"};
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_PRODUCT_BY_CATEGORY, queryParameters: param);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  getAllProduct() async {
    for(var item in data["categories"]){
      data["items_${item.id}"] = ProductCategoryApplication.fromJson((await getProductByCategory(item.id)).data).products;
    }
  }
}