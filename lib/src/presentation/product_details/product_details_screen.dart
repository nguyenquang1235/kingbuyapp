import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';

import '../presentation.dart';

class ProductDetailsScreen extends StatefulWidget {
  final Product params;
  const ProductDetailsScreen(this.params);

  @override
  _ProductDetailsScreenState createState() => _ProductDetailsScreenState();
}

class _ProductDetailsScreenState extends State<ProductDetailsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ProductDetailsViewModel>(
      viewModel: ProductDetailsViewModel(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Container(
            child: widget.params.isInstallment == 1
                ? Center(
                    child: FlatButton(
                      onPressed: () => _toInstallmentPage(),
                      child: Text("Đặt Mua Trả Góp"),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          side: BorderSide(width: 0.1)),
                      color: AppColors.primary,
                    ),
                  )
                : Center(
                    child: Text("Không mua trả góp"),
                  ),
          ),
        );
      },
    );
  }

  _toInstallmentPage(){
    Navigator.pushNamed(context, Routers.installment, arguments: widget.params);
  }
}
