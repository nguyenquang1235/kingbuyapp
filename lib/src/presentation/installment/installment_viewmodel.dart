import 'package:flutter/cupertino.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';
import 'package:kingbuy_app/src/resource/model/installment/bank/bank_application.dart';
import 'package:kingbuy_app/src/resource/model/installment/credit_card/credit_application.dart';
import 'package:kingbuy_app/src/resource/repo/installment_repository.dart';
import 'package:kingbuy_app/src/resource/resource.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

class InstallmentViewModel extends BaseViewModel{

  final InstallmentRepository repository;
  final Product product;
  InstallmentViewModel({this.repository, this.product});

  final bankController = BehaviorSubject<int>();
  final creditCardController = BehaviorSubject<int>();
  final installmentMonthController = BehaviorSubject<int>();
  final installmentPrepaysController = BehaviorSubject<int>();
  final prepayPriceController = BehaviorSubject<double>();

  int _bankId;
  int _installmentCardType;
  int _monthsInstallment;
  int _prepayInstallment;


  init() async {
    Map<String, dynamic> data = {
      "banks": BankApplication.fromJson((await repository.getAllBanks()).data).data,
      "credit_cards": CreditApplication.fromJson((await repository.getAllCreditCard()).data).data,
    };
    chooseBank(data["banks"].first.id);
    chooseCreditCard(data["credit_cards"].first.id);
    chooseInstallmentMonth(int.parse(product.installmentMonths.first));
    chooseInstallmentPrepay(int.parse(product.installmentPrepay.first));
    return data;
  }

  chooseBank(int id){
    bankController.sink.add(id);
    _bankId = id;
  }

  chooseCreditCard(int id){
    creditCardController.sink.add(id);
    _installmentCardType = id;
  }

  chooseInstallmentMonth(int id){
    installmentMonthController.sink.add(id);
    _monthsInstallment = id;
  }

  chooseInstallmentPrepay(int id){
    installmentPrepaysController.sink.add(id);
    double price = (product.salePrice??product.price)/100*id;
    prepayPriceController.sink.add(price);
    _prepayInstallment = id;
  }

  submitInstallment() async {
    Map<String, dynamic> params = {
      "product_id" : product.id,
      "installment_type":1,
      "bank_id": _bankId,
      "installment_card_type" : _installmentCardType,
      "months_installment" : _monthsInstallment,
      "prepay_installment" : _prepayInstallment,
    };
    var result = await repository.createInstallment(params);
    if(result.status == 1){
      Navigator.pushReplacementNamed(context, Routers.installment_details, arguments: result.data["id"]);
    }else {
      Toast.show("Xử lý bị lỗi, vui lòng thử lại", context, duration: 3, gravity: Toast.CENTER);
    }
  }

}