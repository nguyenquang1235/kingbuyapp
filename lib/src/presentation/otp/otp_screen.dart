import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';

class OtpScreen extends StatefulWidget {
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> with TickerProviderStateMixin {

  RegisterViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<RegisterViewModel>(
      viewModel: RegisterViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel,
      builder: (context, viewModel, child) {
        NextPageAnimation(vsync: this);
        return Scaffold(
          appBar: AppBar(
            leading: Icon(Icons.arrow_back,color: AppColors.light),
            title: Text("Nhập mã xác nhận", style: AppStyles.DEFAULT_LARGE.copyWith(color: AppColors.light),),
            backgroundColor: AppColors.primary,
          ),
          body: AnimatedBuilder(
            animation: NextPageAnimation.animation,
            builder: (context, child) {
              return Transform.scale(scale: NextPageAnimation.animation.value, child: child,);
            },
            child: Container(
              color: AppColors.light,
              child: Text("adasdasd"),
            ),
          ),
        );
      },
    );
  }
}
