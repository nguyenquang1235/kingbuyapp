import 'promotion.dart';

class PromotionApplication {
  List<Promotion> _promotion;
  List<Promotion> get promotions => _promotion;

  PromotionApplication({List<Promotion> promotion}) {
    _promotion = promotion;
  }

  factory PromotionApplication.fromJson(Map<String, dynamic> json){
    return PromotionApplication(
      promotion: List<Promotion>.from(
          json["news"].map((x) => Promotion.fromJson(x))),
    );
  }


}
