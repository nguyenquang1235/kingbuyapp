import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:kingbuy_app/src/presentation/home/home.dart';
import 'package:kingbuy_app/src/resource/model/model.dart';
import 'package:kingbuy_app/src/resource/model/promotion/promotion.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import '../presentation.dart';
import 'package:intl/intl.dart';

class HomeScreen extends StatefulWidget {
  final User user;

  const HomeScreen({this.user});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // HomeViewModel _viewModel;
  final carouselIndex = BehaviorSubject<int>();

  // List<Promotion> _promotions;
  // List<MyPromotion> _myPromotions;
  // List<Product> _newProducts;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<HomeViewModel>(
        viewModel: HomeViewModel(repository: Provider.of(context)),
        builder: (context, viewModel, child) {
          return Container(
            child: Column(
              children: [
                _buildHeader(),
                Expanded(
                  child: _buildBody(viewModel.repository.data),
                )
              ],
            ),
          );
        });
  }

  _buildHeader() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      color: Colors.blue,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Container(
              child: Image.asset(AppImages.user3),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(100))),
            ),
            flex: 1,
          ),
          Expanded(
            flex: 5,
            child: Container(
              padding: const EdgeInsets.only(left: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "${widget.user.name}",
                    style: AppStyles.DEFAULT_MEDIUM
                        .copyWith(color: AppColors.light),
                  ),
                  Text("điểm ${widget.user.rewardPoint ?? 0}",
                      style: AppStyles.DEFAULT_LARGE
                          .copyWith(color: AppColors.light)),
                ],
              ),
            ),
          ),
          Expanded(
            child: Icon(
              Icons.search,
              color: AppColors.light,
              size: 30,
            ),
          ),
          Expanded(
            child: Image.asset(
              AppImages.placeholder2,
              width: 30,
              height: 30,
            ),
          ),
          Expanded(
            child: Icon(Icons.card_giftcard, color: AppColors.light, size: 30),
          ),
          Expanded(
            child: Image.asset(
              AppImages.cart,
              width: 30,
              height: 30,
            ),
          ),
        ],
      ),
    );
  }

  _buildBody(dynamic data) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _buildPromotionsCarouselSlider(data["promotions"]),
          _buildCategoriesWrap(data["categories"]),
          _buildMyPromotionsCarouselSlider(data["my_promotions"]),
          _buildNewProductListView(data["new_products"]),
          _buildCategoryProduct(data["categories"], data),
        ],
      ),
    );
  }

  _buildListPromotionItem(List<Promotion> promotions) {
    return promotions
        .map((e) => Container(
              width: MediaQuery.of(context).size.width,
              child: Image.network(
                AppEndpoint.BASE_URL + e.imageSource,
                fit: BoxFit.fill,
                loadingBuilder: (context, child, loadingProgress) {
                  if (loadingProgress == null) return child;
                  return LoadingBouncingLine.circle(
                    size: 50,
                    backgroundColor: Colors.grey,
                  );
                },
              ),
            ))
        .toList();
  }

  _buildPromotionsCarouselSlider(List<Promotion> promotions) {
    carouselIndex.sink.add(1);
    return AspectRatio(
      aspectRatio: 2,
      child: Container(
        child: Stack(
          children: [
            CarouselSlider(
                items: _buildListPromotionItem(promotions),
                options: CarouselOptions(
                  enlargeStrategy: CenterPageEnlargeStrategy.height,
                  aspectRatio: 2,
                  viewportFraction: 1.0,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                  autoPlay: true,
                  autoPlayAnimationDuration: const Duration(milliseconds: 1500),
                  onPageChanged: (index, reason) {
                    carouselIndex.sink.add(index);
                  },
                )),
            Align(
              alignment: Alignment(1, 1),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: promotions.map((e) {
                  int index = promotions.indexOf(e);
                  return Container(
                    child: StreamBuilder(
                        stream: carouselIndex.stream,
                        builder: (context, snapshot) {
                          return Container(
                            width: 10.0,
                            height: 10.0,
                            margin: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              shape: BoxShape.circle,
                              color: snapshot.data == index
                                  ? Colors.white
                                  : Colors.transparent,
                            ),
                          );
                        }),
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildListCategoryItem(List<Category> categories) {
    return categories
        .map((e) => Container(
              height: 90,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ClipRRect(
                    child: Image.network(
                      AppEndpoint.BASE_LOGIN + e.imageSource,
                      fit: BoxFit.fill,
                      width: 60,
                      height: 60,
                      loadingBuilder: (context, child, loadingProgress) {
                        if (loadingProgress == null) return child;
                        return LoadingBouncingLine.circle(
                          size: 50,
                          backgroundColor: Colors.grey,
                        );
                      },
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  Container(
                    width: 80,
                    child: Text(
                      e.name.toUpperCase(),
                      style: AppStyles.DEFAULT_SMALL_BOLD,
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ))
        .toList();
  }

  _buildCategoriesWrap(List<Category> categories) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Danh muc sản phẩm",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        Container(
          margin: const EdgeInsets.symmetric(vertical: 16),
          child: Wrap(
            children: _buildListCategoryItem(categories),
            spacing: 10,
            runSpacing: 10,
          ),
        ),
      ],
    );
  }

  _buildListMyPromotionItem(List<MyPromotion> promotions) {
    return promotions
        .map((e) => Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                child: Image.network(
                  AppEndpoint.BASE_LOGIN + e.imageSource,
                  fit: BoxFit.fill,
                  loadingBuilder: (context, child, loadingProgress) {
                    if (loadingProgress == null) return child;
                    return LoadingBouncingLine.circle(
                      size: 50,
                      backgroundColor: Colors.grey,
                    );
                  },
                ),
              ),
            ))
        .toList();
  }

  _buildMyPromotionsCarouselSlider(List<MyPromotion> promotions) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Ưu đãi dành cho bạn",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        CarouselSlider(
            items: _buildListMyPromotionItem(promotions),
            options: CarouselOptions(
              enlargeStrategy: CenterPageEnlargeStrategy.height,
              aspectRatio: 2.25,
              viewportFraction: 1,
              // enableInfiniteScroll: false,
              disableCenter: true,
              enlargeCenterPage: false,
              scrollDirection: Axis.vertical,
              autoPlay: true,
              autoPlayAnimationDuration: const Duration(milliseconds: 1000),
            ))
      ],
    );
  }

  _buildListNewProductItem(List<Product> data) {
    return data.map((e) {
      return Container(
        width: MediaQuery.of(context).size.width * 0.6,
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 4),
        margin: const EdgeInsets.all(2),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 1))
            ],
            border:
                Border.all(width: 0.5, color: Colors.grey.withOpacity(0.5))),
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.35,
                  height: MediaQuery.of(context).size.width * 0.35,
                  child: Image.network(
                    AppEndpoint.BASE_LOGIN + e.imageSource,
                    fit: BoxFit.fill,
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) return child;
                      return LoadingBouncingLine.circle(
                        size: 50,
                        backgroundColor: Colors.grey,
                      );
                    },
                  ),
                ),
                ReadMoreText(
                  e.name.toUpperCase(),
                  trimLines: 2,
                  trimMode: TrimMode.Line,
                  trimCollapsedText: '...',
                  colorClickableText: Colors.black,
                  style: AppStyles.DEFAULT_MEDIUM_BOLD,
                  textAlign: TextAlign.start,
                ),
                Align(
                  alignment: Alignment(-1, 0),
                  child: Text(
                    "${e.brandName}",
                    style: AppStyles.DEFAULT_SMALL_BOLD
                        .copyWith(color: AppColors.primary),
                  ),
                ),
                Align(
                  alignment: Alignment(-1, 0),
                  child: SmoothStarRating(
                      allowHalfRating: false,
                      starCount: 5,
                      rating: double.parse(e.star.toString()),
                      size: 15,
                      isReadOnly: true,
                      filledIconData: Icons.star,
                      halfFilledIconData: Icons.star_half,
                      color: Colors.orange,
                      borderColor: Colors.orange,
                      spacing: 0.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${NumberFormat(",###", "vi").format(e.price)}",
                          style: AppStyles.DEFAULT_MEDIUM_BOLD
                              .copyWith(color: AppColors.danger),
                        ),
                        Text(
                          "đ",
                          style: AppStyles.DEFAULT_SMALL_BOLD
                              .copyWith(color: AppColors.danger),
                        ),
                      ],
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${NumberFormat(",###", "vi").format(e.salePrice)}",
                          style: AppStyles.DEFAULT_SMALL_BOLD.copyWith(
                              color: AppColors.dark,
                              decoration: TextDecoration.lineThrough),
                        ),
                        Text(
                          "đ",
                          style: AppStyles.DEFAULT_VERY_SMALL_BOLD.copyWith(
                              color: AppColors.dark,
                              decoration: TextDecoration.lineThrough),
                        )
                      ],
                    )
                  ],
                ),
                e.gifts.isNotEmpty
                    ? Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Image.asset(AppImages.gift1), flex: 2,),
                          Expanded(child: SizedBox(), flex: 1,),
                          Expanded(
                            flex: 9,
                            child: ReadMoreText(
                              "Tặng ${e.gifts.first.name}",
                              trimLines: 1,
                              trimMode: TrimMode.Line,
                              trimCollapsedText: '...',
                              colorClickableText: Colors.black,
                              style: AppStyles.DEFAULT_SMALL_BOLD,
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ],
            ),
            Align(
              alignment: Alignment(1, -1),
              child: e.saleOff != 0
                  ? Container(
                      width: 40,
                      height: 20,
                      alignment: Alignment(0, 0),
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          color: Colors.red),
                      child: Text(
                        "-${e.saleOff}%",
                        style: AppStyles.DEFAULT_MEDIUM
                            .copyWith(color: AppColors.light),
                      ),
                    )
                  : null,
            )
          ],
        ),
      );
    }).toList();
  }

  _buildNewProductListView(List<Product> data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Sản phẩm mới",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        Container(
          height: 270,
          child: ListView(
            children: _buildListNewProductItem(data),
            scrollDirection: Axis.horizontal,
          ),
        ),
      ],
    );
  }

  _buildCategoryProduct(List<Category> categories, dynamic data){
    List<Widget> list = [];
    for(var item in categories){
      list.add(WidgetProductCategory(category: item,products: data["items_${item.id}"],));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: list,
    );
  }

}
