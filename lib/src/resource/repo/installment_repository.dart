import 'package:dio/dio.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/utils/utils.dart';
import '../resource.dart';

class InstallmentRepository{

  Future<NetworkState<dynamic>> getAllBanks() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      Map<String, String> param = {
        "type" : "1",
      };
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_ALL_BANK, queryParameters: param);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> getAllCreditCard() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      response = await AppClients(baseUrl: AppEndpoint.BASE_URL).get(AppEndpoint.GET_ALL_CREDIT_CARD);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> createInstallment(Map<String, dynamic> params) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      response = await AppClients(baseUrl: AppEndpoint.BASE_LOGIN).post(AppEndpoint.CREATE_INSTALLMENT, data: params);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<dynamic>> getInstallmentDetails(int invoiceId) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Response response;
      dynamic params = {
        "invoice_id" : invoiceId
      };
      response = await AppClients(baseUrl: AppEndpoint.BASE_LOGIN).get(AppEndpoint.DETAIL_INSTALLMENT, queryParameters: params);
      return NetworkState.fromJson(response.data);
    } on DioError catch (e) {
      print(e.message);
      return NetworkState.withError(e);
    }
  }

}