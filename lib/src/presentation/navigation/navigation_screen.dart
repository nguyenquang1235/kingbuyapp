import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          "NavigationScreen",
          style:
              AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColors.primary),
        ),
      ),
    );
  }
}
