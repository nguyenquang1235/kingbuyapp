import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/resource/resource.dart';
import 'package:rxdart/rxdart.dart';
import 'package:kingbuy_app/src/presentation/presentation.dart';

class PageViewModel extends BaseViewModel {
  final pageController = BehaviorSubject<HomePage>();
  final widgetController = BehaviorSubject<Widget>();

  User _user;

  Stream<HomePage> get pageStream => pageController.stream;
  Sink<HomePage> get pageSink => pageController.sink;

  Stream<Widget> get widgetStream => widgetController.stream;
  Sink<Widget> get widgetSink => widgetController.sink;

  init(User param) {
    _user = param;
    pushPage(HomePage.home);
  }

  pushPage(HomePage value) {
    List<StatefulWidget> list = [
      HomeScreen(user: _user),
      CategoryScreen(),
      NotificationScreen(),
      AccountScreen(),
    ];
    switch (value) {
      case HomePage.home:
        widgetSink.add(list[0]);
        break;
      case HomePage.category:
        widgetSink.add(list[1]);
        break;
      case HomePage.notification:
        widgetSink.add(list[2]);
        break;
      case HomePage.account:
        widgetSink.add(list[3]);
        break;
    }
    pageSink.add(value);
  }
}
