class CreditCard {
  CreditCard({
    this.id,
    this.name,
    this.imageSource,
  });

  int id;
  String name;
  String imageSource;

  factory CreditCard.fromJson(Map<String, dynamic> json) => CreditCard(
    id: json["id"],
    name: json["name"],
    imageSource: json["image_source"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image_source": imageSource,
  };
}