
class User{

  int _id;
  String _name;
  String _email;
  String _phoneNumber;
  String _avatarSource;
  String _dateOfBirth;
  int _gender;
  int _status;
  String _deviceToken;
  String _createAt;
  String _updateAt;
  String _memberCardNumber;
  int _rewardPoint;

  int get id => _id;
  String get name => _name;
  String get email => _email;
  String get phoneNumber => _phoneNumber;
  String get avatarSource => _avatarSource;
  String get dateOfBirth => _dateOfBirth;
  int get gender => _gender;
  int get status => _status;
  String get deviceToken => _deviceToken;
  String get createAt => _createAt;
  String get updateAt => _updateAt;
  String get memberCardNumber => _memberCardNumber;
  int get rewardPoint => _rewardPoint;

  User({
    int id,
    String name,
    String email,
    String phoneNumber,
    String avatarSource,
    String dateOfBirth,
    int gender,
    int status,
    String deviceToken,
    String createAt,
    String updateAt,
    String memberCardNumber,
    int rewardPoint,
  }) {
    _id = id;
    _name = name;
    _email = email;
    _phoneNumber = phoneNumber;
    _avatarSource = avatarSource;
    _dateOfBirth = dateOfBirth;
    _gender = gender;
    _status = status;
    _deviceToken = deviceToken;
    _createAt = createAt;
    _updateAt = updateAt;
    _memberCardNumber = memberCardNumber;
    _rewardPoint = _rewardPoint;
  }

  factory User.formJson(Map<String, dynamic> data){
    return User(
      id:  data["profile"]["id"],
      name:  data["profile"]["name"],
      email:  data["profile"]["email"],
      phoneNumber:  data["profile"]["phone_number"],
      avatarSource:  data["profile"]["avatar_source"],
      dateOfBirth:  data["profile"]["date_of_birth"],
      gender:  data["profile"]["gender"],
      status:  data["profile"]["status"],
      deviceToken:  data["profile"]["device_token"],
      createAt:  data["profile"]["create_at"],
      updateAt:  data["profile"]["update_at"],
      memberCardNumber: data["member_card_number"],
      rewardPoint: data["reward_point"]
    );
  }

  Map<String, dynamic> toJson(){
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] =_name;
    map['email'] = _email;
    map['phone_number'] = _phoneNumber;
    map['avatar_source'] = _avatarSource;
    map['date_of_birth'] = _dateOfBirth;
    map['gender'] = _gender;
    map['status'] = _status;
    map['device_token'] = _deviceToken;
    map['create_at'] = _createAt;
    map['update_at'] = _updateAt;
    map['member_card_number'] = _memberCardNumber;
    map['reward_point'] = _rewardPoint;
    return map;
  }
}