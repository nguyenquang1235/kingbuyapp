import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/configs/configs.dart';
import 'package:kingbuy_app/src/presentation/base/base.dart';
import 'package:provider/provider.dart';

import '../presentation.dart';

class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with TickerProviderStateMixin {
  LoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    NextPageAnimation(vsync: this);
    return BaseWidget<LoginViewModel>(
      viewModel: LoginViewModel(
        repository: Provider.of(context),
        account: TextEditingController(),
        password: TextEditingController(),
        context: context
        // pageAnimation: Provider.of(context),
      ),
      onViewModelReady: (viewModel){
        _viewModel = viewModel;
      },
      builder: (context, viewModel, child) {
        NextPageAnimation(vsync: this);
        return Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Container(
            padding: const EdgeInsets.symmetric(vertical: 24),
            color: Colors.white,
            child: AnimatedBuilder(
              animation: NextPageAnimation.animation,
              builder: (context, child) {
                return Transform.scale(scale: NextPageAnimation.animation.value, child: child,);
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 3,
                    child: _buildHeader(),
                  ),
                  Expanded(
                    flex: 5,
                    child: _buildBody(),
                  ),
                  Expanded(
                    flex: 2,
                    child: _buildFooter(),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  _buildHeader() {
    return Container(
      child: Column(
        children: [
          Image.asset(AppImages.logo),
          Text("ĐĂNG NHẬP",
              style:
                  AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: AppColors.dark))
        ],
      ),
    );
  }

  _buildBody() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        children: [
          Form(
            key: _viewModel.formKey,
            child: Column(
              children: [
                WidgetInput(
                  hint: "Email hoặc số Điện thoại",
                  hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.grey),
                  frontIcon:
                  Icon(Icons.person_outline, color: AppColors.grey, size: 40),
                  elevation: 3,
                  inputController: _viewModel.account,
                  // validator: (value) => _viewModel.validateAccount(value),
                ),
                WidgetInput(
                  hint: "Mật khẩu",
                  hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.grey),
                  frontIcon: Image.asset(
                    AppImages.lock,
                    height: 30,
                    width: 40,
                    color: AppColors.grey,
                  ),
                  elevation: 3,
                  inputController: _viewModel.password,
                  inputType: TextInputType.visiblePassword,
                  validator: (value) => _viewModel.validatePass(value),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                child: Text(
                  "Quên mật khẩu ?",
                  style: AppStyles.DEFAULT_MEDIUM,
                ),
                onTap: () => _viewModel.forgotPass(),
              ),
              FlatButton(
                child: Text(
                  "Đăng Nhập",
                  style:
                      AppStyles.DEFAULT_MEDIUM.copyWith(color: AppColors.light),
                ),
                color: AppColors.danger,
                padding:
                    const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                onPressed: () {
                  if(_viewModel.formKey.currentState.validate()){
                    _viewModel.login(LoginType.Account);
                  }
                },
              )
            ],
          ),
        ],
      ),
    );
  }

  _buildFooter() {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: Text(
              "Hoặc đăng nhập bằng",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
          ),
          Expanded(
            child: Container(
              width: 120,
              height: 40,
              child: Row(
                children: [
                  Expanded(
                    child: GestureDetector(
                      child: Image.asset(AppImages.facebook),
                      onTap: () => _viewModel.login(LoginType.Facebook),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      child: Image.asset(AppImages.google),
                      onTap: () => _viewModel.login(LoginType.Google),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Không có tài khoản ? ",
                  style: AppStyles.DEFAULT_MEDIUM_BOLD,
                ),
                GestureDetector(
                  child: Text("Đăng ký",
                      style: AppStyles.DEFAULT_MEDIUM_BOLD
                          .copyWith(decoration: TextDecoration.underline)),
                  onTap: () => _viewModel.registerAcc(),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
