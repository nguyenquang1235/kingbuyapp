import 'package:flutter/material.dart';
import 'package:kingbuy_app/src/presentation/base/base.dart';

import '../routers.dart';

class RegisterViewModel extends BaseViewModel{

  loginAcc(){
    Navigator.pushReplacementNamed(context, Routers.login);
  }

  registerAcc(){
    Navigator.pushNamed(context, Routers.otp);
  }
}