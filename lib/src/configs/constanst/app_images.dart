class AppImages {
  AppImages._();

  static const String BASE = 'assets/images/';

  static final String user1 = '$BASE-e-_Group_@2x.jpg';
  static final String user2 = '${BASE}user (1)@2x.png';
  static final String user3 = '${BASE}user@2x.png';
  static final String save = '$BASE-e-_Group_@2x.png';
  static final String refresh = '$BASE-e-_Path_ + _Compound Path_@2x.png';
  static final String aboutUs = '$BASE-e-about-us@2x.png';
  static final String phone1 = '$BASE-e-phone@2x.png';
  static final String phone2 = '${BASE}phone@2x.png';
  static final String alarm = '${BASE}alarm@2x.png';
  static final String coupon = '${BASE}coupon@2x.png';
  static final String eye = '${BASE}eye@2x.png';
  static final String faceMess = '${BASE}facebook-messenger-13164@2x.png';
  static final String gift1 = '${BASE}gift (1)@2x.png';
  static final String redPoint = '${BASE}Group 3@2x.png';
  static final String redCircle = '${BASE}Group 23@2x.png';
  static final String gift2 = '${BASE}Group 177@2x.png';
  static final String home = '${BASE}home@2x.png';
  static final String lock = '${BASE}lock@2x.png';
  static final String icLauncher = '${BASE}logo.png';
  static final String logo = '${BASE}LOGO-01@2x.png';
  static final String menu = '${BASE}menu@2x.png';
  static final String placeholder1 = '${BASE}pin@2x.png';
  static final String placeholder2 = '${BASE}placeholder@2x.png';
  static final String cart = '${BASE}shopping-cart@2x.png';
  static final String star = '${BASE}star (1)@2x.png';
  static final String success = '${BASE}success@2x.png';
  static final String surface = '${BASE}surface1@2x.png';
  static final String zaloChat = '${BASE}zalo-chat-logo-png-300@2x.png';
  static final String flash = '${BASE}flashscreen.png';
  static final String facebook = '${BASE}facebook.png';
  static final String google = '${BASE}google.png';
}
