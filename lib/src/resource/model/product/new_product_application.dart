import 'package:kingbuy_app/src/resource/model/product/product.dart';

class NewProductApplication {
  NewProductApplication({
    this.data,
  });

  List<Product> data;

  // factory ProductApplication.fromJson(dynamic json) =>
  //     ProductApplication(
  //       data: List<Product>.from(json.map((x) => Product.fromJson(x))),
  //     );
  //

  factory NewProductApplication.fromJson(dynamic json) {
    List<Product> list = [];
    for (var item in json) {
      list.add(Product.fromJson(item));
    }
    list.removeAt(0);
    return NewProductApplication(data: list);
  }

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}
