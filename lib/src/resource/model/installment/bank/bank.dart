class Bank {
  Bank({
    this.id,
    this.name,
    this.imageSource,
  });

  int id;
  String name;
  String imageSource;

  factory Bank.fromJson(Map<String, dynamic> json) => Bank(
    id: json["id"],
    name: json["name"],
    imageSource: json["image_source"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "image_source": imageSource,
  };
}