class Gift {
  Gift({
    this.id,
    this.name,
    this.price,
    this.imageSource,
    this.description,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String name;
  int price;
  String imageSource;
  dynamic description;
  DateTime createdAt;
  DateTime updatedAt;

  factory Gift.fromJson(Map<String, dynamic> json) => Gift(
    id: json["id"],
    name: json["name"],
    price: json["price"],
    imageSource: json["image_source"],
    description: json["description"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "price": price,
    "image_source": imageSource,
    "description": description,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
