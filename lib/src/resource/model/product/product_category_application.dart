import '../../resource.dart';

class ProductCategoryApplication{
  ProductCategoryApplication({
    this.id,
    this.name,
    this.products,
    this.recordsTotal,
  });

  int id;
  String name;
  List<Product> products;
  int recordsTotal;

  factory ProductCategoryApplication.fromJson(Map<String, dynamic> json) => ProductCategoryApplication(
    id: json["id"],
    name: json["name"],
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    recordsTotal: json["recordsTotal"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
    "recordsTotal": recordsTotal,
  };
}