import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

class NextPageAnimation{
  TickerProvider vsync;
  static CurvedAnimation animation;
  AnimationController animationController;

  NextPageAnimation({this.vsync}){
    animationController = AnimationController(vsync: vsync, duration: const Duration(milliseconds: 650));
    animationController.forward();
    animation = CurvedAnimation(
      curve: Curves.easeIn,
      parent: animationController
    );
  }
}